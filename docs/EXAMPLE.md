# LaserTag usage example

## Installation
*composer require starxen/lasertag*

Make sure the bundle is correctly added in your project
```php
// config/bundles.php
StarXen\LaserTag\LaserTagBundle::class => ['all' => true]
```

## Usage
### 1. Create LaserTag plugin

```php
// src/LaserTag/MyPlugin.php
<?php

declare(strict_types=1);

namespace App\LaserTag;

use StarXen\LaserTag\LaserTag\AbstractPlugin;
use StarXen\LaserTag\LaserTag\LaserTag;
use StarXen\LaserTag\LaserTag\TagAttribute;

class MyPlugin extends AbstractPlugin
{
    public function getTags(): array
    {
        return [
            new LaserTag('h2', [$this, 'h2'], [new TagAttribute('class')]), // The attribute is not required by default.
            new LaserTag('youtube', [$this, 'youtube'], [new TagAttribute('url', true), new TagAttribute('title')])
        ];
    }

    public function h2(string $content): string
    {
        return <<<HTML
<h2 class="app-color-darken fw-bolder mt-3 mb-3 {$this->getAttribute('class')}">$content</h2>
HTML;
    }

    public function youtube(): string
    {
        return <<<HTML
<div class="app-border-left-bold embed-responsive embed-responsive-16by9 col col-lg-6 mx-auto mt-3 mb-3">
    <iframe class="embed-responsive-item" src="{$this->getAttribute('url')}" title="{$this->getAttribute('title')}" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>
HTML;
    }

}
```

### 2. Render template by LaserTagService

```php
    #[Route('/', name: 'app_index')]
    public function home(LaserTagService $laserTagService): Response
    {
        $rendered = $laserTagService->render('
            [h2 class="extra-class-name"]Check my new YouTube video![/h2]
            [youtube url="https://www.youtube.com/watch?v=dQw4w9WgXcQ"/]
        ');
        return new Response($rendered);
    }
```

## Get loaded tags
If you need to get an array of loaded tags and attribute information, you can use the function below.
```php
// Service: \StarXen\LaserTag\Service\LaserTagService
$tags = $laserTagService->getAvailableTags();
```

## Twig integration
You can render template inside Twig template. Bundle provide laser_tag_render filter.
```twig
{{ '[h2 class="extra-class-name"]Check my new YouTube video![/h2]
        [youtube url="https://www.youtube.com/watch?v=dQw4w9WgXcQ"/]' | laser_tag_render }}

```

## Create LaserTag plugins in your bundle
If you need to enable the LaserTag plugin to be loaded from your bundle, use the following config.
[Make sure the services.yaml is correctly loaded](https://symfony.com/doc/current/bundles/extension.html)
```yaml
# vendor/you/yourbundle/Resources/config/services.yaml
services:
    # Plugins location: vendor/you/yourbundle/src/LaserTag
    You\YourBundle\LaserTag\:
        autowire: true
        autoconfigure: true
        resource: '../../src/LaserTag/*'
```
