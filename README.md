# LaserTag

LaserTag is a Symfony bundle that allows you to write and render your own HTML templates based on your own BBCode-style tags.
By using LaserTag, we gain reusability and independence of the content from the accompanying HTML code.

### Example

#### Before render

```lasertag
[h2 class="extra-class-name"]Check my new YouTube video![/h2]
[youtube url="https://www.youtube.com/watch?v=dQw4w9WgXcQ"/]
```

#### After render
```html
<h2 class="app-color-darken fw-bolder mt-3 mb-3 extra-class-name">Check my new YouTube video!</h2>
<div class="app-border-left-bold embed-responsive embed-responsive-16by9 col col-lg-6 mx-auto mt-3 mb-3">
    <iframe class="embed-responsive-item" src="https://www.youtube.com/watch?v=dQw4w9WgXcQ" title="" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>
```

#### LaserTag Plugin
```php
// src/LaserTag/MyPlugin.php
<?php

declare(strict_types=1);

namespace App\LaserTag;

use StarXen\LaserTag\LaserTag\AbstractPlugin;
use StarXen\LaserTag\LaserTag\LaserTag;
use StarXen\LaserTag\LaserTag\TagAttribute;

class MyPlugin extends AbstractPlugin
{
    public function getTags(): array
    {
        return [
            new LaserTag('h2', [$this, 'h2'], [new TagAttribute('class')]),
            new LaserTag('youtube', [$this, 'youtube'], [new TagAttribute('url', true), new TagAttribute('title')])
        ];
    }

    public function h2(string $content): string
    {
        return <<<HTML
<h2 class="app-color-darken fw-bolder mt-3 mb-3 {$this->getAttribute('class')}">$content</h2>
HTML;
    }

    public function youtube(): string
    {
        return <<<HTML
<div class="app-border-left-bold embed-responsive embed-responsive-16by9 col col-lg-6 mx-auto mt-3 mb-3">
    <iframe class="embed-responsive-item" src="{$this->getAttribute('url')}" title="{$this->getAttribute('title')}" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>
HTML;
    }
    
}
```

## Installation
*composer require starxen/lasertag*

## Usage

See [EXAMPLE.md](docs/EXAMPLE.md)
