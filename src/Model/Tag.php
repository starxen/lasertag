<?php

declare(strict_types=1);

namespace StarXen\LaserTag\Model;

class Tag
{

    private string $name = '';
    private array $subTags = [];
    private string $content = '';
    private array $attributes = [];

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Tag
    {
        $this->name = $name;

        return $this;
    }

    public function getSubTags(): array
    {
        return $this->subTags;
    }

    public function setSubTags(array $subTags): Tag
    {
        $this->subTags = $subTags;

        return $this;
    }

    public function addSubTag(Tag $subTag): Tag
    {
        $this->subTags[] = $subTag;

        return $this;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): Tag
    {
        $this->content = $content;

        return $this;
    }

    public function getAttributes(): array
    {
        return $this->attributes;
    }

    public function setAttributes(array $attributes): Tag
    {
        $this->attributes = $attributes;

        return $this;
    }

}
