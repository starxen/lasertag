<?php

declare(strict_types=1);

namespace StarXen\LaserTag\Twig;

use StarXen\LaserTag\Service\LaserTagService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class LaserTagExtension extends AbstractExtension
{

    public function __construct(private readonly LaserTagService $laserTagService)
    {
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('laser_tag_render', [$this, 'render'], ['is_safe' => ['html']])
        ];
    }

    public function render(string $text): string
    {
        return $this->laserTagService->render($text);
    }

}
