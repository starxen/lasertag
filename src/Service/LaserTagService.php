<?php

declare(strict_types=1);

namespace StarXen\LaserTag\Service;

use StarXen\LaserTag\Exception\UnknownTagException;
use StarXen\LaserTag\Model\Tag;
use DOMDocument;
use DOMElement;
use DOMText;
use Symfony\Component\DependencyInjection\Attribute\AutowireIterator;

class LaserTagService
{
    private const string LEFT_SQUARE_BRACKET = 'LASER_TAG_LEFT_SQUARE_BRACKET';
    private const string RIGHT_SQUARE_BRACKET = 'LASER_TAG_RIGHT_SQUARE_BRACKET';
    private const string DOCUMENT = '#document';
    private const string MAIN = 'main';

    private array $tags = [];

    public function __construct(#[AutowireIterator('laser_tag.plugin')] private readonly iterable $plugins)
    {
    }

    public function render(string $text): string
    {
        $this->loadPlugins();
        $text = $this->prepareText($text);

        $document = new DOMDocument();
        $document->loadXML($text);
        $tag = $this->parseTag($document);

        return $this->renderTag($tag);
    }

    public function getAvailableTags(): array
    {
        $this->loadPlugins();
        return array_column($this->tags, 'tag');
    }

    private function parseTag(DOMDocument|DOMElement|DOMText $node): Tag
    {
        $data = new Tag();

        if ($node->nodeType === XML_TEXT_NODE) {
            $data->setContent(htmlspecialchars($node->nodeValue));
        } else {
            $data->setName($node->nodeName);
        }

        $attributes = [];
        if ($node->hasAttributes()) {
            foreach ($node->attributes as $attribute) {
                $attributes[$attribute->name] = $attribute->value;
            }
        }
        $data->setAttributes($attributes);

        if ($node->hasChildNodes()) {
            foreach ($node->childNodes as $child) {
                $data->addSubTag($this->parseTag($child));
            }
        }

        return $data;
    }

    private function renderTag(Tag $tag): string
    {
        $output = $tag->getContent();
        foreach ($tag->getSubTags() as $subTag) {
            $output .= $this->renderTag($subTag);
        }
        return $this->renderTemplate($tag->getName(), $tag->getAttributes(), $output);
    }

    private function renderTemplate(string $tagName, array $attributes, string $content): string
    {
        switch ($tagName) {
            case self::DOCUMENT:
            case self::MAIN:
            case '':
                return $content;
            default:
                if (!array_key_exists($tagName, $this->tags)) {
                    throw new UnknownTagException($tagName);
                }
                $this->tags[$tagName]['plugin']->setAttributes($attributes);
                $this->tags[$tagName]['plugin']->setTag($this->tags[$tagName]['tag']);
                return $this->tags[$tagName]['tag']->getCallable()($content);
        }
    }

    private function loadPlugins(): void
    {
        if (!empty($this->tags)) {
            return;
        }
        foreach ($this->plugins as $plugin) {
            $tags = $plugin->getTags();
            foreach ($tags as $tag) {
                $this->tags[$tag->getName()] = ['tag' => $tag, 'plugin' => $plugin];
            }
        }
    }

    private function prepareText(string $text): string
    {
        $text = '[' . self::MAIN . ']' . $text . '[/' . self::MAIN . ']';
        $text = str_replace(['[[', ']]'], [self::LEFT_SQUARE_BRACKET, self::RIGHT_SQUARE_BRACKET], $text);
        $text = htmlspecialchars($text, ENT_SUBSTITUTE);
        $text = str_replace(['[', ']'], ['<', '>'], $text);
        return str_replace([self::LEFT_SQUARE_BRACKET, self::RIGHT_SQUARE_BRACKET], ['[', ']'], $text);
    }

}
