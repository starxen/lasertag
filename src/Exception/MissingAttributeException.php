<?php

declare(strict_types=1);

namespace StarXen\LaserTag\Exception;

use StarXen\LaserTag\LaserTag\LaserTag;
use Exception;

class MissingAttributeException extends Exception
{

    public function __construct(private readonly string $name, private readonly LaserTag $tag)
    {
        parent::__construct('Required attribute "' . $name . '" on Tag "[' . $tag->getName() . ']" is missing!');
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getTag(): LaserTag
    {
        return $this->tag;
    }

}
