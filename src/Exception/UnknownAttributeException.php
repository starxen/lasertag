<?php

declare(strict_types=1);

namespace StarXen\LaserTag\Exception;

use StarXen\LaserTag\LaserTag\LaserTag;
use Exception;

class UnknownAttributeException extends Exception
{

    public function __construct(private readonly string $name, private readonly LaserTag $tag)
    {
        parent::__construct('Unknown attribute "' . $name . '" on Tag "[' . $tag->getName() . ']"');
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getTag(): LaserTag
    {
        return $this->tag;
    }

}
