<?php

declare(strict_types=1);

namespace StarXen\LaserTag\Exception;

use Exception;

class UnknownTagException extends Exception
{

    public function __construct(private readonly string $tagName)
    {
        parent::__construct('Unknown tag: "' . $tagName . '"');
    }

    public function getTagName(): string
    {
        return $this->tagName;
    }

}
