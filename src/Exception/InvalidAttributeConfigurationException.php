<?php

declare(strict_types=1);

namespace StarXen\LaserTag\Exception;

use Exception;

class InvalidAttributeConfigurationException extends Exception
{

    public function __construct(private readonly string $name, private readonly mixed $value)
    {
        parent::__construct('Invalid attribute configuration on Tag  "[' . $name . ']". Expected an array containing TagAttribute objects, but received a variable of type' . gettype($this->value) . ' in configuration array.');
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getValue(): mixed
    {
        return $this->value;
    }
}
