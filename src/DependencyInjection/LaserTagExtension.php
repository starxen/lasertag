<?php

declare(strict_types=1);

namespace StarXen\LaserTag\DependencyInjection;

use StarXen\LaserTag\LaserTag\PluginInterface;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Extension\Extension;

class LaserTagExtension extends Extension
{

    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'config'));
        $loader->load('services.yaml');

        $container->registerForAutoconfiguration(PluginInterface::class)->addTag('laser_tag.plugin');
    }
}
