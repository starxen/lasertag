<?php

declare(strict_types=1);

namespace StarXen\LaserTag\LaserTag;

use StarXen\LaserTag\Exception\InvalidAttributeConfigurationException;

class LaserTag
{
    private mixed $callable;

    public function __construct(private readonly string $name, callable $callable, private readonly array $attributes = [])
    {
        $this->callable = $callable;
        foreach ($this->attributes as $attribute) {
            if (!($attribute instanceof TagAttribute)) {
                throw new InvalidAttributeConfigurationException($name, $attribute);
            }
        }
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCallable(): callable
    {
        return $this->callable;
    }

    /** @return TagAttribute[] */
    public function getAttributes(): array
    {
        $attributes = [];
        foreach ($this->attributes as $attribute) {
            $attributes[$attribute->getName()] = $attribute;
        }
        return $attributes;
    }
}
