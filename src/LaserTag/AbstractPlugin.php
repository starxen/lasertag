<?php

declare(strict_types=1);

namespace StarXen\LaserTag\LaserTag;

use StarXen\LaserTag\Exception\MissingAttributeException;
use StarXen\LaserTag\Exception\UnknownAttributeException;

abstract class AbstractPlugin implements PluginInterface
{
    private array $attributes = [];

    private ?LaserTag $tag = null;

    public function getAttributes(): array
    {
        return $this->attributes;
    }

    public function setAttributes(array $attributes): void
    {
        $this->attributes = $attributes;
    }

    public function setTag(LaserTag $laserTag): void
    {
        $this->tag = $laserTag;
    }

    protected function getAttribute(string $name): ?string
    {
        if (!array_key_exists($name, $this->tag->getAttributes())) {
            throw new UnknownAttributeException($name, $this->tag);
        }
        if (!array_key_exists($name, $this->attributes) && $this->tag->getAttributes()[$name]->isRequired()) {
            throw new MissingAttributeException($name, $this->tag);
        }
        return $this->attributes[$name] ?? null;
    }
}
