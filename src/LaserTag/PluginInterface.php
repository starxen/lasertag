<?php

declare(strict_types=1);

namespace StarXen\LaserTag\LaserTag;

interface PluginInterface
{
    public function getTags(): array;
}
