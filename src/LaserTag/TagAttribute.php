<?php

declare(strict_types=1);

namespace StarXen\LaserTag\LaserTag;

readonly class TagAttribute
{
    public function __construct(private string $name, private bool $required = false)
    {
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function isRequired(): bool
    {
        return $this->required;
    }

}
